import java.util.ArrayList;
import java.util.Arrays;

public class Solution {

    public static ArrayList<Double> findMedian(ArrayList<Integer> arr, int n, int m) {
        ArrayList<Double> medians = new ArrayList<>();

        // Sliding window approach
        for (int i = 0; i <= n - m; i++) {
            ArrayList<Integer> subarray = new ArrayList<>(arr.subList(i, i + m)); // Get subarray of size 'm'
            subarray.sort(null); // Sort the subarray

            if (m % 2 == 0) { // If size is even
                double median = (subarray.get(m / 2 - 1) + subarray.get(m / 2)) / 2.0; // Calculate median
                medians.add(median);
            } else { // If size is odd
                double median = subarray.get(m / 2); // Get the middle element
                medians.add(median);
            }
        }

        return medians;
    }

    public static void main(String[] args) {
        // Example usage
        ArrayList<Integer> arr1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Double> result1 = findMedian(arr1, 4, 3);
        System.out.println(result1); // Output: [2.0, 3.0]

        ArrayList<Integer> arr2 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        ArrayList<Double> result2 = findMedian(arr2, 4, 4);
        System.out.println(result2); // Output: [2.5]

        ArrayList<Integer> arr3 = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 4, 3, 2, 1));
        ArrayList<Double> result3 = findMedian(arr3, 8, 4);
        System.out.println(result3); // Output: [2.5, 3.5, 3.5, 3.5, 2.5]
    }
}
