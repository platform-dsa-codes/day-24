//Kadane's Algorithm
class Solution {
    public int maxSubArray(int[] nums) {
        int maxSum = nums[0]; // Initialize maxSum with the first element
        int currentSum = nums[0]; // Initialize currentSum with the first element
        
        // Iterate through the array starting from the second element
        for (int i = 1; i < nums.length; i++) {
            // Update currentSum to either continue the current subarray or start a new one
            currentSum = Math.max(nums[i], currentSum + nums[i]);
            
            // Update maxSum if the currentSum is greater
            maxSum = Math.max(maxSum, currentSum);
        }
        
        return maxSum;
    }
    
    public static void main(String[] args) {
        Solution solution = new Solution();
        
        // Example usage:
        int[] nums1 = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        System.out.println(solution.maxSubArray(nums1)); // Output: 6
        
        int[] nums2 = {1};
        System.out.println(solution.maxSubArray(nums2)); // Output: 1
        
        int[] nums3 = {5, 4, -1, 7, 8};
        System.out.println(solution.maxSubArray(nums3)); // Output: 23
    }
}
